@echo off
set VCXSRV_PATH=C:\Program Files\VcXsrv
set VCXSRV_ARGS=-wgl -nolisten inet6 -keyhook -unixkill -xkblayout de

if "%1" == "-status" (
  tasklist /FI "IMAGENAME eq vcxsrv.exe" 2>NUL | find /I /N "vcxsrv.exe">NUL
  if "%ERRORLEVEL%" == "0" (
    echo running
  ) else (
    echo not running
  )
) else (
  if "%1%" == "-kill" (
    "C:\Windows\System32\taskkill.exe" /f /im "vcxsrv.exe"
  ) else (
      if "%1" == "-multiwindow" (
        "%VCXSRV_PATH%\vcxsrv.exe" :0 -screen 0 @1 %VCXSRV_ARGS% -trayicon -multiwindow -silent-dup-error
      ) else (
		 if "%1" == "-xinerama" (
		  "%VCXSRV_PATH%\vcxsrv.exe" :0 -screen 0 @1 %VCXSRV_ARGS% -notrayicon -nodecoration -resize=none +xinerama -screen 1 @2 %VCXSRV_ARGS% -notrayicon -nodecoration -resize=none +xinerama
		) else (
		  "%VCXSRV_PATH%\vcxsrv.exe" :0 -screen 0 @1 %VCXSRV_ARGS% -notrayicon -nodecoration -resize=none
		)
      )
  )
)
